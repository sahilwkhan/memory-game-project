

const cardsGifs = [
    {
        name:"1.gif",
        img: "../gifs/1.gif"
    },
    {
        name:"2.gif",
        img: "../gifs/2.gif"
    },
    {
        name:"3.gif",
        img: "../gifs/3.gif"
    },
    {
        name:"4.gif",
        img: "../gifs/4.gif"
    },
    {
        name:"5.gif",
        img: "../gifs/5.gif"
    },
    {
        name:"6.gif",
        img: "./gifs/6.gif"
    },
    {
        name:"7.gif",
        img: "../gifs/7.gif"
    },
    {
        name:"8.gif",
        img: "../gifs/8.gif"
    },
    {
        name:"9.gif",
        img: "../gifs/9.gif"
    },
    {
        name:"10.gif",
        img: "../gifs/10.gif"
    }
];    

const tempArray = [...cardsGifs];
console.log(tempArray);

tempArray.sort(()=>{
    return 0.5-Math.random();
})
const newCardGifs = cardsGifs.concat(tempArray);


cardsGifs.sort(()=>{
    return 0.5-Math.random();
})

const gameContainer = document.getElementById("grid");
const startButton = document.getElementById("start");


let cardSelectedName = [];
let cardSelectedId = [];

let cardsMatched = [];
const score = document.getElementById("result");
score.innerText = 0;
const flips = document.getElementById("flips");
flips.innerText = 0;

let clickCount = 0;
let isChecking = false;

// Create Cards
function createCards(){
    const gifElements = newCardGifs.map( (gif,index)=>{
        let card = document.createElement("img");
        card.setAttribute("src",`../gifs/first.png`);
        card.setAttribute("image-id", index);
        card.setAttribute("class","end");
        card.setAttribute("name", gif.name);
        card.addEventListener("click",flipCard);
        
        return card;
    });    
    gameContainer.append(...gifElements);;
}    

function endGame(scoreCount){
    document.querySelector(".details").style.display = 'none';
    let bestScore;
    let highScore = localStorage.getItem('score');
    if (highScore){
        if (parseInt(highScore)> clickCount){
            console.log(typeof highScore);
        }
        else{
            localStorage.setItem("score",clickCount);
        }
    }
    else{
        localStorage.setItem('score',0);
    }
    gameContainer.innerHTML = `Congratulations, you Won. Your Final Score is ${flips.innerText}.<br><br>Best Score : ${highScore}`;
    gameContainer.style.color = 'red';
    gameContainer.style.textAlign = 'center';


    const clickEventListenerFunction = (event) => {
    if (event.target.classList.contains('end')) {
        event.target.removeEventListener('click', clickEventListenerFunction);
    }
    };
    gameContainer.addEventListener('click', clickEventListenerFunction);
    
}

//check for matching cards
function checkedSelectedCards(){
    let cards = document.querySelectorAll('img');

    const firstCard = cardSelectedId[0];
    const secondCard = cardSelectedId[1];


    if(cardSelectedName[0] === cardSelectedName[1] && (firstCard!= secondCard)){

        console.log("cardSelectedID = ",cardSelectedId, "---->matched.");
        cardsMatched.push(...cardSelectedId);
    }
    else if(firstCard == secondCard){
        console.log("DOUBLE CLICKED");
        cardSelectedName = [];
        cardSelectedId = [];
        isChecking = false;
        cards[firstCard].setAttribute('src',"../gifs/first.png");
        return;
    }
    else{   
        cards[firstCard].setAttribute('src',"../gifs/first.png");
        cards[firstCard].style.transition = 1;
        cards[secondCard].setAttribute('src',"../gifs/first.png");
    }
    cardSelectedName = [];
    cardSelectedId = [];
    
    score.innerText = cardsMatched.length/2;
    if(cardsMatched.length === newCardGifs.length){
        console.log("OVER");
        isChecking =false;
        endGame(flips.innerText);
        document.querySelector(".game").style
        return;
    }

    isChecking = false;
}


// flip card function 
function flipCard(){
    if (isChecking){
        return;
    }
    else{

        if (cardsMatched.includes( this.getAttribute("image-id")) || cardSelectedId.includes( this.getAttribute("image-id"))) {
            return;
        } 
        else{
            clickCount++;
            flips.innerText = clickCount;
    
            let cardIndex = this.getAttribute("image-id");
            
            cardSelectedName.push(newCardGifs[cardIndex].name);
    
            cardSelectedId.push(cardIndex);
            this.setAttribute('src',newCardGifs[cardIndex].img);
    
            if(cardSelectedId.length === 2){
                isChecking =true;
                setTimeout(checkedSelectedCards,1000);
            }
            else{
                return ;
            }
        }

    }
}


function startGame(){
    
    startButton.addEventListener("click",()=>{
        console.log("OK");
        document.getElementById("start").style.display = 'none';
        document.querySelector(".heading").style.display = 'none';
        document.querySelector(".details").style.display = 'flex';
        document.querySelector(".restart").style.display = 'flex';
        gameContainer.style.display = 'flex';
        createCards();
    })
}
startGame();


document.querySelector(".restart").addEventListener("click",()=>{
    gameContainer.innerHTML ="";
    createCards();
    document.querySelector(".details").style.display = 'flex';
    cardSelectedName = [];
    cardSelectedId = [];
    cardsMatched = [];
    score.innerText = 0;
    flips.innerText = 0;
    clickCount =0;

});
